package ra.starmade;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextPane;
import java.awt.SystemColor;

public class About extends JFrame {
	private static final long serialVersionUID = 4499246339497584468L;
	private JPanel contentPane;
	
	public About() {
		setResizable(false);
		setType(Type.POPUP);
		setTitle("About");
		setBounds(100, 100, 465, 179);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JTextPane txtpnInfo = new JTextPane();
		txtpnInfo.setEditable(false);
		txtpnInfo.setText("Star Made Power Generator Calculator [Version 1.2]\r\n\r\nAll information about this program can be found in the README.md file (which can be opened with any standard text editor).\r\n\r\nSee the included License.txt file for the licensing information.");
		txtpnInfo.setBackground(SystemColor.menu);
		contentPane.add(txtpnInfo, "cell 0 0,grow");
	}

}

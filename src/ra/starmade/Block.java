/*
    Copyright 2015 Brian Milligan

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package ra.starmade;

public class Block {
	private int X, Y, Z;
	
	public Block(int x, int y, int z){
		setX(x);
		setY(y);
		setZ(z);
	}

	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public int getZ() {
		return Z;
	}

	public void setZ(int z) {
		Z = z;
	}
	
	public void setY(int y) {
		Y = y;
	}

	public boolean isNeighbor(Block b){
		if((this.X == b.getX() + 1 || this.X == b.getX() - 1) && this.Y == b.getY() && this.Z == b.getZ())
			return true;
		if((this.Y == b.getY() + 1 || this.Y == b.getY() - 1) && this.X == b.getX() && this.Z == b.getZ())
			return true;
		if((this.Z == b.getZ() + 1 || this.Z == b.getZ() - 1) && this.Y == b.getY() && this.X == b.getX())
			return true;
		return false;
	}
}

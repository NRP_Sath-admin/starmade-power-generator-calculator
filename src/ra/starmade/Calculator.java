/*
 * Copyright 2015 Brian Milligan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ra.starmade;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import net.miginfocom.swing.MigLayout;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Calculator {
	
	private static void draw(Graphics g2) {
		lblLayer.setText("Layer " + Z);
		int WIDTH = canvas.getWidth(), HEIGHT = canvas.getHeight();
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, WIDTH, HEIGHT);
		g2.setColor(Color.BLACK);
		g2.drawRect(0, 0, WIDTH - 1, HEIGHT - 1);
		// Draw grid X lines
		for (int n = 1; n <= WIDTH / 33; n++) {
			g2.drawLine(n * 33, 0, n * 33, HEIGHT - 1);
		}
		// Draw grid Y lines
		for (int n = 1; n <= HEIGHT / 33; n++) {
			g2.drawLine(0, n * 33, WIDTH - 1, n * 33);
		}
		
		ArrayList<Block> temp = new ArrayList<>();
		for (int n = 0; n < systems.size(); n++) {
			for (int l = 0; l < systems.get(n).group.size(); l++) {
				temp.add(systems.get(n).getBlock(l));
			}
		}
		
		// Draw blocks
		for (int x = X_OFFSET * -1; x <= WIDTH / 33 - X_OFFSET; x++) {
			for (int y = Y_OFFSET * -1; y <= HEIGHT / 33 - Y_OFFSET; y++) {
				for (int n = 0; n < temp.size(); n++) {
					Block block = temp.get(n);
					if (block.getX() == x && block.getY() == y && block.getZ() == Z) {
						g2.drawImage(image, x * 33 + 1 + X_OFFSET * 33, y * 33 + 1 + Y_OFFSET * 33, null);
						n = Integer.MAX_VALUE - 1;
					}
				}
			}
		}
		// Draw text
		double mass = 0;
		for (GeneratorSystem gs : systems) {
			mass += gs.group.size();
		}
		
		FontMetrics metrics = g2.getFontMetrics();
		int h = metrics.getHeight();
		int w1 = metrics.stringWidth("Systems: " + systems.size());
		int w2 = metrics.stringWidth("Mass: " + mass / 10);
		
		g2.setColor(Color.CYAN);
		if (w1 > w2) {
			g2.fillRect(1, 1, w1 + 2, h * 2 + 3);
		} else {
			g2.fillRect(1, 1, w2 + 2, h * 2 + 3);
		}
		g2.setColor(Color.BLACK);
		g2.drawString("Systems: " + systems.size(), 2, h);
		g2.drawString("Mass: " + mass / 10, 2, h * 2 + 2);
		g2.dispose();
		canvas.requestFocus();
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BasicConfigurator.configure();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		logger.setLevel(Level.ALL);
		logger.info("\n----------" + dateFormat.format(date) + "----------\nStarting Application...\n");
		logger.info(System.getProperty("java.version"));
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				logger.info("Application closing.");
			}
		});
		
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				try {
					Calculator window = new Calculator();
					aboutWindow = new About();
					window.frmStarMadeGenerator.setVisible(true);
				} catch (Exception e) {
					logger.log(Level.ALL, "Something happened during run time, let's see what it is...\n", e);
					e.printStackTrace();
					e.printStackTrace();
				}
			}
		});
	}
	
	private static void openWebpage(URI uri) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(uri);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private JFrame								frmStarMadeGenerator;
	private JTextField							txtNoB;
	private JFormattedTextField					txtXSize;
	private JFormattedTextField					txtYSize;
	
	private JFormattedTextField					txtZSize;
	private JTextField							txtOutput;
	private JTextField							txtEfficiency;
	static final Logger							logger		= Logger.getLogger(Calculator.class);
	private JPanel								panel;
	
	private JPanel								panel_1;
	
	private static JComponent					canvas;
	private JButton								btnRecenter;
	private static ArrayList<GeneratorSystem>	systems		= new ArrayList<>();
	static int									X_OFFSET	= 0;
	static int									Y_OFFSET	= 0;
	private static BufferedImage				image;
	private static int							Z			= 0;
	private static JLabel						lblLayer;
	private JButton								btnCalculate;
	private JLabel								label;
	private JLabel								label_1;
	private JTextField							txfOutput2;
	private JTextField							txfEfficiency2;
	private JLabel								lblSingleSystemCalculator;
	private JMenuBar							menuBar;
	private JMenu								mnFile;
	private JMenu								mnHelp;
	private JMenuItem							mntmAbout;
	private JMenuItem							mntmStarmadeWiki;
	private JScrollPane							scrollPane;
	private JTextPane							txpInformation;
	private JLabel								lblShipMass;
	private JCheckBox							chckbxInclusive;
	
	private JFormattedTextField					ftfShipMass;
	
	private JMenuItem							mntmClearGrid;
	
	private static About						aboutWindow;
	
	/**
	 * Create the application.
	 */
	public Calculator() {
		initialize();
	}
	
	private void calculateAdvanced() {
		logger.log(Level.INFO, "Starting advanced calculation...");
		double Output = 0;
		double Blocks = 0;
		for (int i = 0; i < systems.size(); i++) {
			logger.log(Level.INFO, "Calculating system[" + (i + 1) + "/" + systems.size() + "]");
			double SumD = systems.get(i).getDepth() + systems.get(i).getHeight() + systems.get(i).getWidth();
			System.out.println("SumD " + SumD);
			Blocks = systems.get(i).group.size();
			Output += 2000000 / (1 + Math.pow(1.000696, -0.333 * Math.pow(SumD / 3, 1.7))) - 1000000 + 25 * Blocks;
		}
		double Efficency = Output / (Blocks * 25);
		txtOutput.setText((double) Math.round(Output * 10) / 10 + "");
		txtEfficiency.setText((double) Math.round(Efficency / systems.size() * 10) + "%");
		specialInformation();
	}
	
	private void calculateBasic() {
		logger.log(Level.INFO, "Starting basic calculation...");
		logger.log(Level.INFO, "NoB: " + txtNoB.getText().isEmpty() + "\nXSize:" + txtXSize.getText().isEmpty() + "\nYSize:" + txtYSize.getText().isEmpty() + "\nZSize:" + txtZSize.getText().isEmpty());
		if (!txtNoB.getText().isEmpty() && !txtXSize.getText().isEmpty() && !txtYSize.getText().isEmpty() && !txtZSize.getText().isEmpty()) {
			double SumD = Double.parseDouble(txtXSize.getText()) + Double.parseDouble(txtYSize.getText()) + Double.parseDouble(txtZSize.getText());
			double Blocks = Double.parseDouble(txtNoB.getText());
			double Output = 0;
			Output = 2000000 / (1 + Math.pow(1.000696, -0.333 * Math.pow(SumD / 3, 1.7))) - 1000000 + 25 * Blocks;
			double Efficency = Output / (Blocks * 25);
			txfOutput2.setText((double) Math.round(Output * 10) / 10 + "");
			txfEfficiency2.setText((double) Math.round(Efficency / systems.size() * 10) + "%");
		}
	}
	
	private void checkNeighborGroups() {
		// Combine neighboring groups
		try {
			for (int g = 0; g < systems.size(); g++) {
				for (int s = 0; s < systems.get(g).group.size(); s++) {
					
					for (int g2 = 0; g2 < systems.size(); g2++) {
						for (int s2 = 0; s2 < systems.get(g2).group.size(); s2++) {
							
							if (g != g2) {// Don't need to check a group against
								// it's self.
								
								if (systems.get(g).getBlock(s).isNeighbor(systems.get(g2).getBlock(s2))) {
									logger.log(Level.DEBUG, "Combine these two groups[" + systems.get(g) + " & " + systems.get(g2) + "]");
									for (Block b : systems.get(g2).group) {
										systems.get(g).addBlock(b);
									}
									systems.remove(g2); // remove old group.
									g = 0; // go back to beginning to make sure no other groups are conjoined.
									g2 = 0;
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, e);
		}
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			logger.log(Level.ERROR, "Something happened while setting the look and feel", e);
		} catch (ClassNotFoundException e) {
			logger.log(Level.ERROR, "Something happened while setting the look and feel", e);
		} catch (InstantiationException e) {
			logger.log(Level.ERROR, "Something happened while setting the look and feel", e);
		} catch (IllegalAccessException e) {
			logger.log(Level.ERROR, "Something happened while setting the look and feel", e);
		}
		
		try {
			image = ImageIO.read(Calculator.class.getResource("/ra/img/PowerTank32px.png"));
		} catch (IOException e) {
			logger.log(Level.ERROR, "Graphics Error while loading image...", e);
			e.printStackTrace();
		}
		
		frmStarMadeGenerator = new JFrame();
		frmStarMadeGenerator.setTitle("Star Made Power Generator Calculator");
		frmStarMadeGenerator.setBounds(100, 100, 760, 452);
		frmStarMadeGenerator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStarMadeGenerator.getContentPane().setLayout(new MigLayout("", "[][grow][50px,grow]", "[47.00,grow][][][]"));
		
		panel = new JPanel();
		frmStarMadeGenerator.getContentPane().add(panel, "cell 0 0,grow");
		panel.setLayout(new MigLayout("", "[][grow]", "[][][][][][][][]"));
		
		lblSingleSystemCalculator = new JLabel("Single System Calculator");
		panel.add(lblSingleSystemCalculator, "cell 0 0");
		
		JLabel lblNumberOfBlocks = new JLabel("Number of Blocks");
		panel.add(lblNumberOfBlocks, "cell 0 1");
		
		txtNoB = new JTextField();
		txtNoB.setText("0");
		panel.add(txtNoB, "cell 1 1");
		txtNoB.setColumns(10);
		
		JLabel lblXSize = new JLabel("X Size");
		panel.add(lblXSize, "cell 0 2");
		
		txtXSize = new JFormattedTextField();
		txtXSize.setText("0");
		panel.add(txtXSize, "cell 1 2");
		txtXSize.setColumns(10);
		
		JLabel lblYSize = new JLabel("Y Size");
		panel.add(lblYSize, "cell 0 3");
		
		txtYSize = new JFormattedTextField();
		txtYSize.setText("0");
		panel.add(txtYSize, "cell 1 3");
		txtYSize.setColumns(10);
		
		JLabel lblZSize = new JLabel("Z Size");
		panel.add(lblZSize, "cell 0 4");
		
		txtZSize = new JFormattedTextField();
		txtZSize.setText("0");
		panel.add(txtZSize, "cell 1 4");
		txtZSize.setColumns(10);
		
		btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calculateBasic();
			}
		});
		panel.add(btnCalculate, "cell 0 5");
		
		label = new JLabel("Output");
		panel.add(label, "cell 0 6,alignx trailing");
		
		txfOutput2 = new JTextField();
		txfOutput2.setEditable(false);
		txfOutput2.setColumns(10);
		panel.add(txfOutput2, "cell 1 6,growx");
		
		label_1 = new JLabel("Efficiency");
		panel.add(label_1, "cell 0 7,alignx trailing");
		
		txfEfficiency2 = new JTextField();
		txfEfficiency2.setEditable(false);
		txfEfficiency2.setColumns(10);
		panel.add(txfEfficiency2, "cell 1 7,growx");
		
		panel_1 = new JPanel();
		frmStarMadeGenerator.getContentPane().add(panel_1, "cell 1 0,grow");
		panel_1.setLayout(new BorderLayout(0, 0));
		
		canvas = new JComponent() {
			private static final long	serialVersionUID	= -6545747889403297209L;
			
			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				draw(g);
			}
		};
		canvas.setBackground(Color.WHITE);
		
		canvas.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				// Move "camera" down and redraw display.
				if (arg0.getKeyCode() == KeyEvent.VK_DOWN) {
					Y_OFFSET++;
					canvas.repaint();
				}
				// Move "camera" up and redraw display.
				if (arg0.getKeyCode() == KeyEvent.VK_UP) {
					Y_OFFSET--;
					canvas.repaint();
				}
				// Move "camera" left and redraw display.
				if (arg0.getKeyCode() == KeyEvent.VK_LEFT) {
					X_OFFSET--;
					canvas.repaint();
				}
				// Move "camera" right and redraw display.
				if (arg0.getKeyCode() == KeyEvent.VK_RIGHT) {
					X_OFFSET++;
					canvas.repaint();
				}
				// Move "camera" down one layer and redraw display.
				if (arg0.getKeyCode() == KeyEvent.VK_PAGE_DOWN) {
					Z--;
					canvas.repaint();
				}
				// Move "camera" up one layer and redraw display.
				if (arg0.getKeyCode() == KeyEvent.VK_PAGE_UP) {
					Z++;
					canvas.repaint();
				}
			}
		});
		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int X = arg0.getX() / 33 - X_OFFSET, Y = arg0.getY() / 33 - Y_OFFSET;// Convert to grid coordinates and apply offset.
				logger.log(Level.INFO, "Clicked at(x:" + X + " y:" + Y + " z: " + Z + ")");
				boolean exists = false;
				int s = -1, b = -1;
				
				// Loop through the all GeneeratorSystems and compare the blocks to the location that was clicked.
				for (int n = 0; n < systems.size(); n++) {
					for (int l = 0; l < systems.get(n).group.size(); l++) {
						
						Block block = systems.get(n).getBlock(l);
						
						if (block.getX() == X && block.getY() == Y && block.getZ() == Z) {// If a block is in the same location as the clicked location.
							exists = true;// Block exists here.
							s = n;// index of the GeneratorSystem that contains
							// the block.
							b = l;// index of the block.
						}
					}
				}
				if (exists) {// Remove block that has been clicked on.
					systems.get(s).remove(b);// Remove the block.
					if (systems.get(s).group.isEmpty()) {// Is the system empty.
						systems.remove(s); // If so remove empty system.
					} else {// Otherwise make sure it is still one group.
						GeneratorSystem temp = systems.get(s);// hold the
						// GeneratorSystem
						// temporarily.
						systems.remove(s);// Remove old GeneratorSystem.
						SplitGroup(temp);// Check the GeneratorSystem.
					}
					
				} else {// Create a new block at X, Y, Z
					Block newBlock = new Block(X, Y, Z);// Make the new block.
					boolean newSystem = true;// By default a new GeneratorSystem
					// will be made.
					if (systems.size() > 0) {// If there are other
						// GeneratorSystems.
						boolean done = false;// escape variable for the for
						// loops.
						// Check each GeneratorSystem for a block that is next
						// to the new block.
						for (int n = 0; n < systems.size() && !done; n++) {
							for (int l = 0; l < systems.get(n).group.size() && !done; l++) {
								Block block = systems.get(n).getBlock(l);// Block
								// to
								// be
								// checked.
								if (block.isNeighbor(newBlock)) {// Check if
									// blocks
									// are
									// neighbors.
									logger.log(Level.DEBUG, "Block added to existing system " + systems.get(n).toString());
									systems.get(n).addBlock(newBlock);// Add
									// block
									// to
									// system.
									done = true;// Escape for loops.
									newSystem = false;// Do not create a new
									// GeneratorSystem.
								}
							}
						}
						if (newSystem && systems.size() != 0) {// If other
							// GeneratorSystem
							// exist but a
							// new
							// GeneratorSystem
							// needs to be
							// made.
							GeneratorSystem reVal = new GeneratorSystem();// New
							// GeneratorSystem.
							reVal.addBlock(newBlock);// Add new Block to the new
							// GeneratorSystem.
							logger.log(Level.DEBUG, "Block added to new system " + reVal.toString());
							systems.add(reVal);// Add the new GeneratorSystem to
							// the ArrayList holding all
							// GeneratorSystem.
						}
					} else if (systems.size() == 0) {// If this is the first
						// block to be placed
						// when no other
						// GeneratorSystem
						// exist.
						GeneratorSystem reVal = new GeneratorSystem();// Create
						// new
						// GeneratorSystem.
						reVal.addBlock(newBlock);// Add new Block to the new
						// GeneratorSystem.
						logger.log(Level.DEBUG, "Block added to new system " + reVal.toString());
						systems.add(reVal);// Add the new GeneratorSystem to the
						// ArrayList holding all
						// GeneratorSystem.
					}
				}
				// For debugging and logging purposes.//
				String reVal = "\n";
				for (GeneratorSystem system : systems) {
					reVal += "System " + system.toString() + "\n";
					for (Block block : system.group) {
						reVal += "\t[" + block.getX() + "," + block.getY() + "," + block.getZ() + "]\n";
					}
				}
				logger.log(Level.DEBUG, "Number of systems:" + systems.size() + reVal);
				
				checkNeighborGroups();// Check if groups should be joined
				// together.
				canvas.repaint();// update the display.
				calculateAdvanced();// Calculate new values.
			}
			
		});
		panel_1.add(canvas);
		
		scrollPane = new JScrollPane();
		frmStarMadeGenerator.getContentPane().add(scrollPane, "cell 2 0,grow");
		
		txpInformation = new JTextPane();
		txpInformation.setEditable(false);
		txpInformation.setText("Total Mass: 0\r\n\r\nCan Support Cloaker: false\r\nRemaining Energy: 0 e/s\r\n\r\nCan Support Jammer: false\r\nRemaining Energy: 0 e/s\r\n\r\nCan Support Both: false\r\nRemaining Energy: 0 e/s");
		scrollPane.setViewportView(txpInformation);
		
		btnRecenter = new JButton("Recenter");
		frmStarMadeGenerator.getContentPane().add(btnRecenter, "flowx,cell 1 1");
		btnRecenter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				X_OFFSET = 0;
				Y_OFFSET = 0;
				canvas.repaint();
			}
		});
		
		lblShipMass = new JLabel("Ship Mass");
		frmStarMadeGenerator.getContentPane().add(lblShipMass, "flowx,cell 2 1");
		
		JLabel lblOutput = new JLabel("Output");
		frmStarMadeGenerator.getContentPane().add(lblOutput, "cell 0 2,alignx trailing");
		
		txtOutput = new JTextField();
		txtOutput.setEditable(false);
		frmStarMadeGenerator.getContentPane().add(txtOutput, "cell 1 2,growx");
		txtOutput.setColumns(10);
		
		chckbxInclusive = new JCheckBox("Inclusive");
		chckbxInclusive.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				specialInformation();
			}
		});
		chckbxInclusive.setSelected(true);
		chckbxInclusive.setToolTipText("If checked, the mass of the genertor design will be included.");
		frmStarMadeGenerator.getContentPane().add(chckbxInclusive, "cell 2 2");
		
		JLabel lblEfficiency = new JLabel("Efficiency");
		frmStarMadeGenerator.getContentPane().add(lblEfficiency, "cell 0 3,alignx trailing");
		
		txtEfficiency = new JTextField();
		txtEfficiency.setEditable(false);
		frmStarMadeGenerator.getContentPane().add(txtEfficiency, "cell 1 3,growx");
		txtEfficiency.setColumns(10);
		
		lblLayer = new JLabel("Layer 0");
		frmStarMadeGenerator.getContentPane().add(lblLayer, "cell 1 1");
		
		ftfShipMass = new JFormattedTextField();
		ftfShipMass.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					specialInformation();
				}
			}
		});
		ftfShipMass.setText("0.0");
		frmStarMadeGenerator.getContentPane().add(ftfShipMass, "cell 2 1,growx");
		
		menuBar = new JMenuBar();
		frmStarMadeGenerator.setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mntmClearGrid = new JMenuItem("Clear Grid");
		mntmClearGrid.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				systems.clear();
				canvas.repaint();
				specialInformation();
			}
		});
		mnFile.add(mntmClearGrid);
		
		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		mntmStarmadeWiki = new JMenuItem("StarMade Wiki");
		mntmStarmadeWiki.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					openWebpage(new URL("http://www.starmadewiki.com/wiki/Main_Page").toURI());
				} catch (URISyntaxException e) {
					e.printStackTrace();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		});
		mnHelp.add(mntmStarmadeWiki);
		
		mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				aboutWindow.setVisible(true);
			}
		});
		mnHelp.add(mntmAbout);
	}
	
	private void specialInformation() {
		boolean sCloaker = false, sJammer = false, sBoth = false;
		double energy = Double.parseDouble(txtOutput.getText()), mass = Double.parseDouble(ftfShipMass.getText()), rCloaker = 0.0, rJammer = 0.0, rBoth = 0.0;
		if (chckbxInclusive.isSelected()) {
			for (GeneratorSystem gs : systems) {
				mass += (double) gs.group.size() / 10;
			}
		}
		
		mass += 0.2;//Account for the shipcore and the cloaker and jammer separately.
		// Check cloaker
		// Thanks to Raisinbat from starmade dock for finding the correct energy cost rate.
		sCloaker = energy >= (mass * 1450);
		rCloaker = (double) Math.round(energy * 10.0) / 10.0 - (mass * 1450);
		
		// Check jammer
		sJammer = energy >= mass * 50.0;
		rJammer = (double) Math.round(energy * 10.0) / 10.0 - mass * 50.0;
		
		mass += 0.1;//Account for the cloaker and jammer together.
		// Check both
		sBoth = energy >= mass * 1500.0;
		rBoth = energy - (mass * 1500.0);
		mass -= 0.2;
		
		txpInformation.setText("Total Mass: " + (double) Math.round(mass * 10) / 10 + "(+0.1 for ship core)" + "\n\nCan Support Cloaker:" + sCloaker + " (Mass:" + (double) Math.round((mass + 0.1) * 10) / 10 + ")\nRemaining Energy: " + (double) Math.round(rCloaker * 10) / 10 + "e/s"
		
		+ "\n\nCan Support Jammer: " + sJammer + " (Mass:" + (double) Math.round((mass + 0.1) * 10) / 10 + ")\nRemaining Energy: " + (double) Math.round(rJammer * 10) / 10 + "e/s"
		
		+ "\n\nCan Support Both: " + sBoth + " (Mass:" + (double) Math.round((mass + 0.2) * 10) / 10 + ")\nRemaining Energy: " + (double) Math.round(rBoth * 10) / 10 + "e/s");
	}
	
	private void SplitGroup(GeneratorSystem generatorSystem) {
		ArrayList<GeneratorSystem> temp = new ArrayList<>();
		GeneratorSystem gs = new GeneratorSystem();
		gs.addBlock(generatorSystem.getBlock(0));
		temp.add(gs);
		
		for (int gsn = 0; gsn < generatorSystem.group.size(); gsn++) {
			Block newBlock = generatorSystem.getBlock(gsn);
			boolean newSystem = true;
			if (systems.size() > 0) {
				boolean done = false;
				for (int n = 0; n < systems.size() && !done; n++) {
					for (int l = 0; l < systems.get(n).group.size() && !done; l++) {
						Block block = systems.get(n).getBlock(l);
						if (block.isNeighbor(newBlock)) {
							logger.log(Level.DEBUG, "Block added to existing system " + systems.get(n).toString());
							systems.get(n).addBlock(newBlock);
							done = true;
							newSystem = false;
						}
					}
				}
				if (newSystem && systems.size() != 0) {
					GeneratorSystem reVal = new GeneratorSystem();
					reVal.addBlock(newBlock);
					logger.log(Level.DEBUG, "Block added to new system " + reVal.toString());
					systems.add(reVal);
				}
			} else if (systems.size() == 0) {
				GeneratorSystem reVal = new GeneratorSystem();
				reVal.addBlock(newBlock);
				logger.log(Level.DEBUG, "Block added to new system " + reVal.toString());
				systems.add(reVal);
			}
		}
	}
}

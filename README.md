# Overview #
![SMPGC_preview.png](https://bitbucket.org/repo/Keaq5p/images/1979799071-SMPGC_preview.png)

Star Made Power Generator Calculator (SMPGC), is a calculator for designing generator systems in Star Made.

# Instructions #
There are two parts to this program the basic calculator and the advanced calculator.

## Basic Calculator ##
With this you input the dimensions of the generator group you want to calculator for, so for example if you have two generators side by side the dimensions would be 1x1x2, the order in which they are inputted is not important, then you put in the number of blocks in the group, and finally hit the button "Calculate" below you will see the output which tells you the e/s it will generate, disregard the "Efficiency" readout as it is more novelty then anything and will probably be removed.

## Advanced Calculator ##
With this you design the group(s) on the grid using the arrow keys, pg up and pg dwn to navigate the building space, click to add or remove a block. Every time a block is added the output at the bottom will be updated as well as the information on the right which tells you a couple things...

1. Total mass with the mass of the ship core included.
2. Can this system by itself (meaning no other blocks, but including the ship core) support a Jammer.
3. How much e/s is left over or (if negative) how much more is needed to support a Jammer.
4. Can this system by itself (meaning no other blocks, but including the ship core) support a Cloaker.
5. How much e/s is left over or (if negative) how much more is needed to support a Cloaker.
6. Can this system by itself (meaning no other blocks, but including the ship core) support both a Jammer and Cloaker.
7. How much e/s is left over or (if negative) how much more is needed to support both a Jammer and Cloaker.

In addition if you want to add in the mass of other blocks you plan to have in your ship you can type in the mass in the "Ship Mass" text box, it will account for the Ship Core, 1 Jammer (when calculated), and 1 Cloaker (when calculated), so you only need put in the mass of the ship you get from the debug info in game.

# Planned Features #
**Schematic Import/Export**

* Allow users to import schematics (this would only import the generator blocks, but would calculate the mass).
* Allow users to export schematics.

**3D View**

* Opens a third window that displays a view of the build, updated in real time.
* Possibly allow users to right/left click to remove/place blocks in the 3D view.
* Possibly make it so users can switch between 2D, and 3D view or have the 3D view be a separate window.

# I Want to Help Develop This! #
Awesome! Have at it!

# ...But I Don't Have any Coding Experience #
No problem! You can help find bugs and suggest improvements. Just use the [Issue Tracker](https://bitbucket.org/NRP_Sath-admin/starmade-power-generator-calculator/issues?status=new&status=open) and submit what bug(s) you have found, just make sure no one else has reported it. Same for suggestions, just follow the rules and all will be good!
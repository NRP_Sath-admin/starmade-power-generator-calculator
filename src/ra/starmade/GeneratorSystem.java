/*
    Copyright 2015 Brian Milligan

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package ra.starmade;

import java.util.ArrayList;

public class GeneratorSystem {
	ArrayList<Block> group = new ArrayList<>();
	public void addBlock(Block block){
		if(!group.contains(block)){
			group.add(block);
		}
	}
	public Block getBlock(int index){
		return group.get(index);
	}
	public ArrayList<Block> getBlocks(){
		return group;
	}
	public void removeBlock(Block object){
		for(Block block: group){
			if(block.equals(object)){
				group.remove(object);
			}
		}
	}
	public void remove(int index) {
		group.remove(index);
	}
	//x
	public int getWidth(){
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
		for(Block block:group){
			if(block.getX() > max)
				max = block.getX();
			if(block.getX() < min)
				min = block.getX();
		}
		System.out.println("Width:" + (max - min + 1));
		return Math.abs(max-min) + 1;
	}
	//y
	public int getHeight(){
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
		for(Block block:group){
			if(block.getY() > max)
				max = block.getY();
			if(block.getY() < min)
				min = block.getY();
		}
		System.out.println("Height:" + (max - min + 1));
		return Math.abs(max-min) + 1;
	}
	//z
	public int getDepth(){
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
		for(Block block:group){
			if(block.getZ() > max)
				max = block.getZ();
			if(block.getZ() < min)
				min = block.getZ();
		}
		System.out.println("Depth:" + (max - min + 1));
		return Math.abs(max-min) + 1;
	}
}
